{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "005ba935",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# PROG 110 · Art of Code · Audio: Python\n",
    "## Session 9: Classes: object-oriented programming\n",
    "## Fall 2023 · November 1\n",
    "### Columbia College Chicago · Audio Arts & Acoustics\n",
    "### Dr. Florian Hollerweger · [✉](mailto:fhollerweger@colum.edu) [☏](https://teams.microsoft.com/l/chat/0/0?users=%3Cfhollerweger@colum.edu%3E) [📅](https://outlook.office365.com/owa/calendar/AudioArtsandAcoustics@office365.colum.edu/bookings/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b8191ce",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Object-oriented programming\n",
    "\n",
    "- One of many programming paradigms\n",
    "- A useful vehicle for managing complexity\n",
    "- Languages associated with OOP:\n",
    "  - [Simula](https://en.wikipedia.org/wiki/Simula): Simula I (1962) & Simula 67 (1967)\n",
    "  - [Smalltalk](https://en.wikipedia.org/wiki/Smalltalk) (1972)\n",
    "  - [C++](https://cplusplus.com/) (1985)\n",
    "  - [Java](https://www.java.com) (1995)\n",
    "  - [SuperCollider](https://supercollider.github.io/) (1996)\n",
    "- Python lends itself to OOP: (almost) everything in Python itself is an object!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6a5c9dc",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Principles\n",
    "\n",
    "- [**Encapsulation**](https://en.wikipedia.org/wiki/Encapsulation_(computer_programming)): Bundling data attributes & methods for operating on them within a *class* (Guttag 20116, p. 126)\n",
    "- [**Information hiding**](https://en.wikipedia.org/wiki/Information_hiding): via private or protected rather than public class attributes, for example (Guttag 20116, p. 126)\n",
    "- [**Polymorphism**](https://www.geeksforgeeks.org/polymorphism-in-python/): Accessing objects of different types through the same interface"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e410d03",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Classes](https://www.w3schools.com/python/python_classes.asp)\n",
    "\n",
    "- A 'blueprint' for creating multiple *instances* of new, user-defined types of *objects*.\n",
    "- *Objects* have *properties* and *methods*.\n",
    "- Common Python convention: Use `PascalCase` for class names"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0daad896",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Create a new class with one property 'x'\n",
    "# NOTE: Executing this code cell will not 'do' anything visible!\n",
    "class MyClass:\n",
    "    x = 5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab85469d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Objects\n",
    "\n",
    "- Properties accessed via *dot notation*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d576d2da",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Create a new object of that class\n",
    "my_object = MyClass()\n",
    "\n",
    "# Classes establish new types of objects\n",
    "print(type(my_object))\n",
    "\n",
    "# Print the property 'x' of this object (dot notation)\n",
    "print(my_object.x) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b191367d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## The `__init__()` function\n",
    "\n",
    "- Virtually all Python classes have a function called `__init__()` (note *double* underscores!)\n",
    "- Executed whenever class is initiated.\n",
    "- Used to assign values to object properties, or other operations required when object is created.\n",
    "- Frequently uses `self` parameter:\n",
    "  - First parameter of any function defined in a class\n",
    "  - Refers to *current instance* of that class\n",
    "  - Used to access variables that belong to class\n",
    "  - Can be named anything you like, but `self` is *very* common and *highly* recommended"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f749a98d",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Class definition\n",
    "class Mic:\n",
    "    def __init__(self, name, price):\n",
    "        self.name = name\n",
    "        self.price = price\n",
    "\n",
    "# Instantiate an object 'mic1' of type 'Mic'\n",
    "mic1 = Mic('Shure SM58', 99.90)\n",
    "\n",
    "# Access properties of our object\n",
    "print(mic1.name)\n",
    "print(mic1.price)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "402d8bc8",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## The `__str__()` function\n",
    "\n",
    "- Controls what should be returned when class object is represented as a string\n",
    "- I.e., tells Python what to do when `print()` is called on an object of the type defined by your class\n",
    "- Makes your classes more useful for the user (easier to debug)\n",
    "- If `__str__()` function is undefined, string representation of the object is returned\n",
    "- More details [here](https://youtu.be/-DP1i2ZU9gk?t=1633)\n",
    "- See [here](https://realpython.com/python-string-formatting/#3-string-interpolation-f-strings-python-36) for details on string formatting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9466907",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Without __str__()\n",
    "\n",
    "class Mic:\n",
    "    def __init__(self, name, price):\n",
    "        self.name = name\n",
    "        self.price = price\n",
    "\n",
    "mic1 = Mic('Shure SM58', 99.90)\n",
    "\n",
    "print(mic1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abc98d1e",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# With __str__()\n",
    "\n",
    "class Mic:\n",
    "    def __init__(self, name, price):\n",
    "        self.name = name\n",
    "        self.price = price\n",
    "        \n",
    "    def __str__(self):\n",
    "        return f'{self.name} (${self.price})'\n",
    "\n",
    "mic1 = Mic('Shure SM58', 99.90)\n",
    "\n",
    "print(mic1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1657e6f9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Methods\n",
    "\n",
    "- Objects can not only have properties , but also *methods* that belong to them.\n",
    "- Defined as functions within a class definition\n",
    "- Accessed via dot notation (like properties)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dd7979e9",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "class Mic:\n",
    "    \n",
    "    def __init__(self, name, price):\n",
    "        # Properties\n",
    "        self.name = name\n",
    "        self.price = price\n",
    "\n",
    "    # Method\n",
    "    def say_hello(self):\n",
    "        print('Hello, my name is', self.name + ',', 'and you can buy me for $', self.price, 'dollars.')\n",
    "\n",
    "mic1 = Mic('Shure SM58', 99.90)\n",
    "mic1.say_hello() "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d9fcbc2",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Modifying objects & their properties"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "632b301c",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Modify the property 'price' of the object 'mic1' of class 'Mic'\n",
    "mic1.price = 79.90\n",
    "\n",
    "# List all properties of 'mic1' object to confirm\n",
    "mic1.__dict__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0603fe62",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Delete the property 'price' of the object 'mic1' of class 'Mic'\n",
    "del mic1.price\n",
    "\n",
    "# List all properties of 'mic1' object to confirm\n",
    "mic1.__dict__"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14859ab8",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Delete the object 'mic1' of class 'Mic' altogether\n",
    "del mic1\n",
    "\n",
    "# Confirm via NameError\n",
    "mic1"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b333b2be",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## [Inheritance](https://www.w3schools.com/python/python_inheritance.asp)\n",
    "\n",
    "- A *child classes* can *inherit* from a *parent* (or: *super*) *class*.\n",
    "- *Multiple inheritance*: inherit from multiple parent classes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "980822b3",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Create a parent (or: super) class"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c77a8a2f",
   "metadata": {},
   "outputs": [],
   "source": [
    "class Mic:\n",
    "    def __init__(self, manufacturer, model ):\n",
    "        self.manufacturer = manufacturer\n",
    "        self.model = model\n",
    "        \n",
    "    def __str__(self):\n",
    "        return f'{self.manufacturer} {self.model}'\n",
    "\n",
    "# Use the Mic class to create an object and call its printname method\n",
    "mic1 = Mic(\"Shure\", \"SM58\")\n",
    "print(mic1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88302222",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Create a child class that inherits from the parent (or: super) class\n",
    "\n",
    "\n",
    "- Class definitions cannot be empty.\n",
    "- But classes without content are possible with a `pass` statement.\n",
    "- Useful to define child classes that\n",
    "  - inherit all properties & methods from their parent class\n",
    "  - without definining their own additional properties or methods."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5d82c949",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "class CondenserMic(Mic):\n",
    "    pass\n",
    "\n",
    "# CondenserMic class inherits all properties and methods from Mic class\n",
    "mic2 = CondenserMic('AKG', 'C414')\n",
    "mic2.print_info() "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d49f2f7d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Add a property that is exclusive to the child class\n",
    "\n",
    "- Presence of `__init__()` method in child class overrides parent class's and therefore disables inheritance\n",
    "- To re-enable inheritance of selected properties from parent class, use `super()` function\n",
    "- But child class can also define its own additional properties & methods.\n",
    "- Example: a `CondenserMic` requires a phantom power voltage, but not *any* `Mic` does.\n",
    "- If parent & child class have identically named methods, inheritance from former is overridden."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7add862e",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "class CondenserMic(Mic):\n",
    "    \n",
    "    # Specify an init function for the child class (which overrides parent's and hence disables inheritance)\n",
    "    def __init__(self, manufacturer, model, voltage):\n",
    "        \n",
    "        # Re-inherit two properties from parent (or: super) class\n",
    "        super().__init__(manufacturer, model)\n",
    "        \n",
    "        # Add a new property specific to the child class\n",
    "        self.phantom = voltage\n",
    "        \n",
    "    # Add a new method specific to the child class (overrides identically named method in parent class)\n",
    "    def __str__(self):\n",
    "        return f'The {self.manufacturer} {self.model} requires a phantom voltage of {self.phantom} volts.'\n",
    "\n",
    "mic3 = CondenserMic('AKG', 'C414', 48)\n",
    "print(mic3)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb8c63c8",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Designing for inheritance\n",
    "\n",
    "https://peps.python.org/pep-0008/#designing-for-inheritance"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  },
  "rise": {
   "autolaunch": true,
   "enable_chalkboard": true,
   "reveal_shortcuts": {
    "main": {
     "fullscreenHelp": "x",
     "toggleOverview": "tab"
    }
   },
   "scroll": true,
   "theme": "serif"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
