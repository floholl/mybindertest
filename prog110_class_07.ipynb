{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "005ba935",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# PROG 110 · Art of Code · Audio: Python\n",
    "## Session 7: Recording, playing & visualizing sound in Python\n",
    "## Fall 2023 · October 18\n",
    "### Columbia College Chicago · Audio Arts & Acoustics\n",
    "### Dr. Florian Hollerweger · [✉](mailto:fhollerweger@colum.edu) [☏](https://teams.microsoft.com/l/chat/0/0?users=%3Cfhollerweger@colum.edu%3E) [📅](https://outlook.office365.com/owa/calendar/AudioArtsandAcoustics@office365.colum.edu/bookings/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "05d2c5de",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [`IPython.display.Audio`](https://ipython.readthedocs.io/en/stable/api/generated/IPython.display.html#IPython.display.Audio): Playing sounds in Jupyter\n",
    "\n",
    "- `Audio` class from `IPython.display` module creates embedded HTML5 audio player\n",
    "- Works *only* in Jupyter Notebooks!\n",
    "- Does *not* work in Spyder, Thonny, IPython consoles outside of Jupyter, etc.\n",
    "- Plays local or remote sound files as well as NumPy arrays\n",
    "- Supports mono, stereo & multi-channel audio\n",
    "- For mono expects 1D NumPy arrays of shape `(N,)`, not 2D arrays of shape `(N,1)`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "be17ff92",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Playing a local sound file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "afd8ea3d",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# HTML5 player (Jupyter Notebooks only)\n",
    "from IPython.display import Audio\n",
    "\n",
    "# Play a local sound file (here: in same directory as .ipynb notebook)\n",
    "Audio('mono.wav', normalize=False)\n",
    "# normalize=False option recommended for designing effects (don't fool your ears!)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf7e594a",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Playing a remote sound file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fee500de",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Play a remote sound file\n",
    "print('Please be patient (takes a while to load)...')\n",
    "Audio('http://ccrma.stanford.edu/~jos/mp3/gtr-jazz-3.mp3', normalize=False)\n",
    "# normalize=False option recommended for designing effects (don't fool your ears!)\n",
    "\n",
    "# More example files: https://ccrma.stanford.edu/~jos/pasp/Sound_Examples.html"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2e46d2c2",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Playing a NumPy array\n",
    "\n",
    "- Note: For mono expects 1D arrays of shape `(N,)`, but will not play 2D arrays of shape `(N,1)`\n",
    "- Potential problem: `sd.rec` from `sounddevices` modules defaults mono to `(N,1)`!\n",
    "- Solution: Use `np.squeeze` to convert to `(N,)`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16455287",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "fs = 44100\n",
    "\n",
    "# Global variables\n",
    "num_seconds = 3 # duration in seconds\n",
    "amplitude = 1/8 # corresponds to -18dB\n",
    "\n",
    "# Generate NumPy array with noise\n",
    "audio = np.random.uniform(-amplitude, amplitude, num_seconds*fs)\n",
    " \n",
    "# HTML5 player (Jupyter Notebooks only)\n",
    "Audio(audio, rate=fs, normalize=False)\n",
    "# normalize=False option recommended for designing effects (don't fool your ears!)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4e6c505",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Soundfile I/O vs. soundcard I/O\n",
    "\n",
    "![Soundcard & soundfile I/O](pics/audio_device_file_io.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5c176893",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Soundfile I/O: the [`soundfile` module](https://pysoundfile.readthedocs.io)\n",
    "\n",
    "- **Anaconda**: does not come pre-installed, so:\n",
    "  1. Start Anaconda Navigator\n",
    "  2. *Environments > Update index...*\n",
    "  3. Search Packages (*Not installed*) > `pysoundfile`\n",
    "  4. Select package and `Apply`\n",
    "- **Thonny**: does not come pre-installed, so:\n",
    "  1. Start Thonny\n",
    "  2. *Tools > Manage packages...*\n",
    "  3. Search on PyPi: `soundfile`\n",
    "  4. Click on `soundfile` (not: `pysoundfile`!) & `Install`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1931bae9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [sf.read](https://pysoundfile.readthedocs.io/en/latest/#soundfile.read): Reading a sound file into a NumPy array\n",
    "\n",
    "- Mono files are read into a 1D array of shape `(N,)`.\n",
    "- Unless `always_2d=True` is passed, in which case they are written to a 2D array of shape `(N,1)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bde1a379",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Import module\n",
    "import soundfile as sf\n",
    "\n",
    "# Read soundfile & get its sample rate\n",
    "audio, fs = sf.read('mono.wav')\n",
    "\n",
    "# This reads the file to a 2D array of shape (N,1) instead of a 1D array of shape (N,).\n",
    "# Note that the Audio class from IPython.display won't be able to play the result!\n",
    "#audio, fs = sf.read('mono.wav', always_2d=True)\n",
    "\n",
    "# Audio data is stored to a NumPy array!\n",
    "print('Type of the audio data:', type(audio))\n",
    "print('Shape of the NumPy array:', audio.shape)\n",
    "print('Sample rate:', fs, 'Hz')\n",
    " \n",
    "# HTML5 player (Jupyter Notebooks only)\n",
    "Audio(audio, rate=fs, normalize=False)\n",
    "# Note: This now plays the stored NumPy array from RAM (not: the file from disc).\n",
    "# normalize=False option recommended for designing effects (don't fool your ears!)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "da85d572",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    " ## Determining duration in seconds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2961663f",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$t = \\frac{n}{f_s}$$\n",
    "\n",
    "- $t$ ... duration [s]\n",
    "- $n$ ... number of samples [1]\n",
    "- $f_s$ ... sample rate [Hz]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5ea1984",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Number of samples and corresponding duration in seconds\n",
    "num_samples = len(audio)\n",
    "duration_sec = len(audio) / fs\n",
    "\n",
    "# Print key data\n",
    "print('Number of samples:', num_samples)\n",
    "print('Corresponding duration:', round(duration_sec,3), 'seconds')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4fa3849c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Determining number of channels\n",
    "\n",
    "- `sf.read` defaults to 1D `(N,)` NumPy arrays for mono files.\n",
    "- Requires case distinction for mono input files (see below).\n",
    "- Or pass `always_2d=True` to `sf.read` to write mono files to 2D `(N,1)` arrays\n",
    "- But then `IPython.display.Audio` won't be able to play them. 😆"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "87ec80a9",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Selection of test files (uncomment only whichever one you'd like to test)\n",
    "audio, fs = sf.read('mono.wav')   # 1-channel file\n",
    "#audio, fs = sf.read('stereo.wav') # 2-channel file\n",
    "#audio, fs = sf.read('octo.wav')   # 8-channel file (not included in GitLab repository, but available on Canvas)\n",
    "\n",
    "if audio.ndim > 1:\n",
    "    num_channels = audio.shape[1]\n",
    "elif audio.ndim == 1:\n",
    "    # NOTE: 'sf.read' by default reads mono files into a (N,) rather than (N,1) NumPy array.\n",
    "    # But audio.shape[1] does not exist if audio.shape is an (N,) tuple!\n",
    "    num_channels = 1\n",
    "\n",
    "# Print key data\n",
    "print('Number of channels:', num_channels)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47883959",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "##  [sf.write](https://pysoundfile.readthedocs.io/en/latest/#soundfile.write): Writing a NumPy array to a sound file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4f539c22",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Generate NumPy array with noise\n",
    "audio_out = np.random.uniform(-amplitude, amplitude, num_seconds*fs)\n",
    "\n",
    "# Write result to new file\n",
    "sf.write('out.wav', audio_out, fs)\n",
    "\n",
    "# Now confirm presence of out.wav file in macOS Finder or Windows Explorer (in same directory as .ipynb notebook)\n",
    "\n",
    "# HTML5 player (Jupyter Notebooks only)\n",
    "Audio('out.wav')\n",
    "# Note: This now plays the written file from disc (not: the stored NumPy array from RAM)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a674d0d",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Soundcard I/O: the [`sounddevice` module](https://python-sounddevice.readthedocs.io)\n",
    "\n",
    "- **Anaconda**: does not come pre-installed, so:\n",
    "  1. Start Anaconda Navigator\n",
    "  2. *Environments > Update index...*\n",
    "  3. Search Packages (*Not installed*) > `python-sounddevice` (from `conda-forge` channel)\n",
    "  4. Select package and `Apply`\n",
    "- **Thonny**: does not come pre-installed, so:\n",
    "  1. Start Thonny\n",
    "  2. *Tools > Manage packages...*\n",
    "  3. Search on PyPi: `sounddevice`\n",
    "  4. Click on `sounddevice` & `Install`\n",
    "- Your mileage on getting this module to work may vary.\n",
    "- However, we will mostly rely on the (very reliable) *`soundfile`* module anyway.\n",
    "- Also, this module *will not work for Jupyter Notebooks launched remotely via Binder*. Why?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e0eb7a7",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Use cases\n",
    "\n",
    "1. Recording sound directly from the sound card (eg., mic) rather than reading it from a sound file.\n",
    "2. Playing a NumPy array as audio directly from a Python IDE (Spyder, Thonny) rather than\n",
    "   - from a Jupyer Notebook using `from IPython.display import Audio`, or\n",
    "   - first writing it to a sound file and then playing that using a regular media player."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84ba9e4c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [sd.rec](https://python-sounddevice.readthedocs.io/en/latest/api/convenience-functions.html#sounddevice.rec): Recording audio from the sound card to a NumPy array\n",
    "\n",
    "- Also check out this [tutorial](https://python-sounddevice.readthedocs.io/en/latest/usage.html#recording).\n",
    "- `soundfile` module reads mono files to 1D `(N,)` NumPy arrays.\n",
    "- But `sounddevice` module records mono signals to 2D `(N,1)` arrays.\n",
    "- So here we can *always* determine number of channels via `.shape[1]` 😀\n",
    "- But on the other hand cannot play `(N,1)` arrays via `IPython.display.Audio`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "83900fcd",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import sounddevice as sd\n",
    "import numpy as np\n",
    "\n",
    "# Basic parameters\n",
    "fs = 44100.0    # Sample rate. TODO: Must be a float for second sd.play example to work below - why?\n",
    "num_seconds = 2 # Duration of recording in seconds\n",
    "\n",
    "# Start recording\n",
    "audio = sd.rec(int(num_seconds * fs), samplerate=fs, channels=1)\n",
    "print('On air...')\n",
    "# NOTE: This will NOT work if you have launched this Jupyter Notebook remotely via Binder! Why?\n",
    "# TODO sp23: Records silence on some machines.\n",
    "\n",
    "# Wait until recording is finished before proceeding (not /always/ required)\n",
    "sd.wait()\n",
    "print('... done.')\n",
    "\n",
    "# Print key information about recorded signal\n",
    "print('The sound has been recorded to a', type(audio))\n",
    "print('Its shape is:', audio.shape)\n",
    "print('Axis #0 of this array reflects the number of samples:', audio.shape[0])\n",
    "print('Axis #1 of this array reflects the number of channels:', audio.shape[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "50b158d4",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# NOTE: This will fail, since IPython.display.Audio does not play 1D (N,1) arrays\n",
    "from IPython.display import Audio\n",
    "\n",
    "# HTML5 player (Jupyter Notebooks only)\n",
    "Audio(audio, rate=fs, normalize=False)\n",
    "# normalize=False option recommended for designing effects (don't fool your ears!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9250e68d",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Solution: Squeeze (N,1) array to (N,) first\n",
    "audio = np.squeeze(audio)\n",
    "\n",
    "# HTML5 player (Jupyter Notebooks only)\n",
    "Audio(audio, rate=fs, normalize=False)\n",
    "# normalize=False option recommended for designing effects (don't fool your ears!)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5101a29e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [sd.play](https://python-sounddevice.readthedocs.io/en/latest/api/convenience-functions.html#sounddevice.play): Playing a NumPy array as audio through the sound card\n",
    "\n",
    "- Also check out this [tutorial](https://python-sounddevice.readthedocs.io/en/latest/usage.html#playback).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90b430d8",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Play previously recorded NumPy array as audio\n",
    "print('On air...')\n",
    "sd.play(audio, fs)\n",
    "\n",
    "# Optional: Wait until recording is finished before proceeding.\n",
    "sd.wait()\n",
    "print('... done.')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "89f6fc3f",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Note that you can play /any/ NumPy array as audio\n",
    "\n",
    "# Global variables\n",
    "num_seconds = 3 # duration in seconds\n",
    "amplitude = 1/8 # corresponds to -18dB\n",
    "\n",
    "# Generate NumPy array with noise\n",
    "audio = np.random.uniform(-amplitude, amplitude, num_seconds*fs)\n",
    "\n",
    "# Play NumPy array as audio (like before)\n",
    "print('On air...')\n",
    "sd.play(audio, fs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6ee00ca",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Channel number vs. NumPy array shape\n",
    "\n",
    "| Module                  | 1 channel (mono)  | ≥2 channels | Comment                                          |\n",
    "|-------------------------|-------------------|-------------|--------------------------------------------------|\n",
    "| `IPython.display.Audio` | `(N,)`            | `(N,C)`     | **will not play `(N,1)`** mono arrays            |\n",
    "| `soundfile.read`        | `(N,)`            | `(N,C)`     | or pass `always_2d=True` to read mono to `(N,1)` |\n",
    "| `soundfile.write `      | `(N,)` or `(N,1)` | `(N,C)`     | accepts both `(N,)` and `(N,1)` arrays as mono   |\n",
    "| `sounddevice.rec`       | **`(N,1)`**       | `(N,C)`     |                                                  |\n",
    "| `soundfile.play`        | `(N,1)` or `(N,)` | `(N,C)`     | accepts both `(N,)` and `(N,1)` arrays as mono   |\n",
    "\n",
    "- `N` ... number of samples\n",
    "- `C` ... number of channels\n",
    "- Advantage of `(N,)` vs. `(N,1)` for mono signals:\n",
    "  - `(N,)`: `IPython.display.Audio` can play it 😀\n",
    "  - `(N,1)`: `.shape[1]` *always* works to count number of channels 😀\n",
    "- See NumPy lecture on how to convert between `(N,)` vs. `(N,1)`:\n",
    "  - Use `np.newaxis` to convert from `(N,)` to `(N,1)`.\n",
    "  - Use `np.squeeze()` to convert from `(N,1)` to `(N,)`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a5c0541a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [`matplotlib.pyplot`](https://matplotlib.org/stable/api/pyplot_summary.html): Visualizing audio in Python\n",
    "\n",
    "- Matplotlib: A very powerful plotting package for Python\n",
    "- Useful in particular for plotting NumPy arrays\n",
    "- Examples:\n",
    "  - Plotting the *waveform* of various audio signals (discussed today)\n",
    "  - In a future lecture we will plot *spectrum* & *spectrogram* of various audio signals\n",
    "- We are particularly interested in the `matplotlib.pyplot` sub-module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7b10df8",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Import Matplotlib's 'pyplot'\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# IPython: uncomment the following line for plots to show\n",
    "#%matplotlib"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "288ccc34",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Where will the resulting plot be displayed?\n",
    "\n",
    "- IPython console:\n",
    "  - Not at all, unless `%matplotlib` [magic command](https://ipython.readthedocs.io/en/stable/interactive/magics.html) is run at beginning\n",
    "  - Once `%matplotlib` has been run, in a new window\n",
    "- Jupyter Notebook:\n",
    "  - Inline, if `%matplotlib` has not been run\n",
    "  - Once `%matplotlib` has been run, in a new window\n",
    "- Spyder: in 'Plots' pane (top right corner)\n",
    "- Thonny: Not at all, unless `plt.show()` is run at end"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b11be1ab",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## `.plot`: Plotting a 1D NumPy array as a mono waveform\n",
    "\n",
    "- Plot waveform as a pseudo-*continuous signal*\n",
    "- Plot amplitude value ($y$ axis) against corresponding sample index ($x$ axis)\n",
    "- A rudimentary solution: always plots *all* samples at *any* zoom factor (*not* how your DAW does it)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "44b55398",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "audio, fs = sf.read('mono.wav')\n",
    "\n",
    "plt.plot(audio)\n",
    "plt.ylim(-1,1) # Set amplitude limit to ±1 to get an idea of level\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8abbda37",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Plotting a 2D NumPy array as an overlaid stereo waveform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8b40aa6",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "audio, fs = sf.read('stereo.wav')\n",
    "left  = audio[:,0]\n",
    "right = audio[:,1]\n",
    "\n",
    "# NOTE: Relies on previously created 'audio' array\n",
    "plt.plot(left)\n",
    "plt.plot(right)\n",
    "plt.ylim(-1,1) # Set amplitude limit to ±1 to get an idea of level\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03d2be09",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Plotting a 2D NumPy array as a stacked stereo waveform"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2090cfcd",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "audio, fs = sf.read('stereo.wav')\n",
    "left  = audio[:,0]\n",
    "right = audio[:,1]\n",
    "\n",
    "# Overall plot\n",
    "fig, (ax1, ax2) = plt.subplots(2, 1)\n",
    "fig.suptitle('A stereo waveform')\n",
    "\n",
    "# Upper subplot\n",
    "ax1.plot(left)\n",
    "ax1.set_ylabel('Left channel')\n",
    "\n",
    "# Set amplitude limit to ±1\n",
    "ax1.set_ylim(-1,1)\n",
    "\n",
    "# Lower subplot\n",
    "ax2.plot(right)\n",
    "ax2.set_ylabel('Right channel')\n",
    "ax2.set_xlabel('Sample index')\n",
    "\n",
    "# Set amplitude limit to ±1\n",
    "ax2.set_ylim(-1,1)\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "566658d0",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Plotting time in seconds rather than sample index on the $x$ axis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "411f3e7d",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "audio, fs = sf.read('stereo.wav')\n",
    "left  = audio[:,0]\n",
    "right = audio[:,1]\n",
    "\n",
    "# Convert sample index to time marker in seconds for plotting\n",
    "time = np.linspace(0, len(audio)/fs, num=len(audio))\n",
    "\n",
    "# Overall plot\n",
    "fig, (ax1, ax2) = plt.subplots(2, 1)\n",
    "fig.suptitle('A stereo waveform')\n",
    "\n",
    "# Upper subplot\n",
    "ax1.plot(time, left)\n",
    "ax1.set_ylabel('Left channel')\n",
    "\n",
    "# Set amplitude limit to ±1\n",
    "ax1.set_ylim(-1,1)\n",
    "\n",
    "# Lower subplot\n",
    "ax2.plot(time, right)\n",
    "ax2.set_ylabel('Right channel')\n",
    "ax2.set_xlabel('Time in seconds')\n",
    "\n",
    "# Set amplitude limit to ±1\n",
    "ax2.set_ylim(-1,1)\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ac0e914",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## `.stem`: Plotting 1D NumPy arrays as mono lollipop graphs\n",
    "\n",
    "- Sometimes we want to focus on the actual individual samples\n",
    "- Requires plotting digital audio as the *discrete signal* that it actually is\n",
    "- Discrete signals traditionally plotted using *lollipop graphs*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "528d2d71",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "audio, fs = sf.read('mono.wav')\n",
    "\n",
    "# Plot samples 12000 through 12019 of previously prepared NumPy array with audio signal\n",
    "plt.stem(audio[12000:12020])\n",
    "plt.ylim(-1,1) # Set amplitude limit to ±1 to get an idea of level\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "rise": {
   "autolaunch": true,
   "enable_chalkboard": true,
   "reveal_shortcuts": {
    "main": {
     "fullscreenHelp": "x",
     "toggleOverview": "tab"
    }
   },
   "scroll": true,
   "theme": "serif"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
