{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "005ba935",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# PROG 110 · Art of Code · Audio: Python\n",
    "## Session 4: Functions, namespaces & scopes\n",
    "## Fall 2023 · September 27\n",
    "### Columbia College Chicago · Audio Arts & Acoustics\n",
    "### Dr. Florian Hollerweger · [✉](mailto:fhollerweger@colum.edu) [☏](https://teams.microsoft.com/l/chat/0/0?users=%3Cfhollerweger@colum.edu%3E) [📅](https://outlook.office365.com/owa/calendar/AudioArtsandAcoustics@office365.colum.edu/bookings/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9790f170",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [Functions](https://www.w3schools.com/python/python_functions.asp)\n",
    "\n",
    "- A coding vehicle for doing things repeatedly.\n",
    "- Functions have to be *defined* before they can be *called*.\n",
    "- Syntax of a *function definition* (pseudo-code):\n",
    "  ```\n",
    "  def function_name(parameter_1, parameter_2, ...):\n",
    "      function body\n",
    "      return return_value\n",
    "  ```\n",
    "- Syntax of a corresponding *function call* (pseudo-code):\n",
    "  ```\n",
    "  my_value = function_name(parameter_1, parameter_2, ...)\n",
    "  ```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae11a37c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Function definitions\n",
    "\n",
    "- Uses `def` keyword (if you don't, it ain't a function!)\n",
    "- Followed by function name (recommendation: `snake_case`)\n",
    "- Followed by `()` with optional parameters\n",
    "- Followed by `:`\n",
    "- Followed by function body (*must* be indented)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0cb8e042",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition\n",
    "def my_function():\n",
    "    print('Hello, world!')\n",
    "\n",
    "# NOTE: Running this code block won't 'do' anything visible."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63d6955c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Function calls\n",
    "\n",
    "- Once a function has been defined, it can be *called* (also multiple times)\n",
    "- Important vehicle of *modular programming* (to be discussed)\n",
    "- Syntax: Name of function, followed by `()` with optional parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbe9666b",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Call previously defined function\n",
    "my_function()\n",
    "\n",
    "# Another function call\n",
    "my_function()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8913120",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Getting values into & out of functions\n",
    "\n",
    "- Functions are an important vehicle for *modular programming* (to be discussed).\n",
    "- One important principle in this context is *encapsulation*.\n",
    "- Function serves as *black box* with its own namespace & scope\n",
    "- Parameters & return values serve as *interface* to the function."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87034e16",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Getting values into & out of functions\n",
    "\n",
    "![Python functions](pics/python_functions.png)\n",
    "\n",
    "1. Main code body passes *argument(s)* to function as *parameter(s)*\n",
    "2. Function further processes value(s) its been passed\n",
    "3. Function `return`s result(s) to main code body\n",
    "4. Main code body further processes *return value(s)*"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "011f6341",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Passing arguments to a function as parameters\n",
    "\n",
    "- We can define functions with *parameters*.\n",
    "- We can pass values to these parameters as *arguments* in our function calls.\n",
    "- Allows us to create more powerful functions that can do different things."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8657c807",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition with parameter\n",
    "def my_function(my_parameter):\n",
    "    print(my_parameter)\n",
    "    \n",
    "# Function  call with argument\n",
    "my_function('Hello, world!')\n",
    "\n",
    "# Function call with different argument\n",
    "my_function('Good bye, world!')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5092382",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Returning values from a function\n",
    "\n",
    "- Uses `return` keyword\n",
    "- Typically concludes function body"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ab29e598",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition\n",
    "def my_function(my_parameter):\n",
    "    my_return_value = my_parameter * 2\n",
    "    return my_return_value\n",
    "    \n",
    "# Function call with argument 3, assigning return value to a variable\n",
    "a = my_function(3)\n",
    "print(a)\n",
    "\n",
    "# Function call with a different argument, assigning return value to a different variable\n",
    "b = my_function(4)\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "496a0de3",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Returning multiple values from a function\n",
    "\n",
    "- In function definition: Single `return` statement with comma-separated list of values\n",
    "- In function call: Assign function to comma-separated list of variables (can be named differently)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d054311c",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Functions can have multiple (comma-separated) return values\n",
    "def foo():\n",
    "    a = 3\n",
    "    b = 4\n",
    "    return a, b # Comma-separate multiple return values\n",
    "\n",
    "# Comma-separate multiple return values also in function call\n",
    "c, d = foo()\n",
    "\n",
    "# Yey, both return values recognized :)\n",
    "print(c)\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c056683e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Default parameter values\n",
    "\n",
    "- One can assign default values to a function's parameters.\n",
    "- Enables us to default to 'meaningful' behavior."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "170f4c47",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition\n",
    "def my_function(my_parameter=5):\n",
    "    my_return_value = my_parameter * 2\n",
    "    return my_return_value\n",
    "    \n",
    "# Function call with argument 3\n",
    "a = my_function(3)\n",
    "print(a)\n",
    "\n",
    "# Function call without an argument assumes default value\n",
    "b = my_function()\n",
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb6aa6ed",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Multiple parameters"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9f7c3129",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition\n",
    "def my_function(my_first_parameter=5, my_second_parameter=4):\n",
    "    my_return_value = my_first_parameter + my_second_parameter\n",
    "    return my_return_value\n",
    "    \n",
    "# Function call with two arguments\n",
    "a = my_function(3,8)\n",
    "print(a)\n",
    "\n",
    "# Function call without arguments assumes defaults\n",
    "b = my_function()\n",
    "print(b)\n",
    "\n",
    "# Function call with only first argument assumes default for second\n",
    "c = my_function(2)\n",
    "print(c)\n",
    "\n",
    "# Arguments can be named explicitly by parameter (for example, to only pass second one, like here)\n",
    "d = my_function(my_second_parameter=2)\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b84408c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Nested function calls\n",
    "\n",
    "- Functions can be *nested*\n",
    "- Example: Two functions that together implement $a^2 \\times b^2$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b929acd",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Function definitions\n",
    "def square(a):\n",
    "    my_square = a**2\n",
    "    return my_square\n",
    "\n",
    "def product(a, b):\n",
    "    my_product = a * b\n",
    "    return my_product\n",
    "\n",
    "# Nested function call\n",
    "product(square(3), square(2))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90f488ca",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Recursive functions\n",
    "\n",
    "- A powerful programming vehicle where a *function calls itself*\n",
    "- In contrast to *iterative* function calls"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9ac524ab",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Recursion example 1: [Factorial](https://en.wikipedia.org/wiki/Factorial) of a non-negative integer $n$\n",
    "\n",
    "- Definition: $n! = n \\cdot (n-1) \\cdot (n-2) \\cdot (n-3) \\cdot \\ldots \\cdot 3 \\cdot 2 \\cdot 1$\n",
    "- Recursive definition: $$n! = n \\cdot (n-1)!$$\n",
    "- For example: $5! = 5 \\cdot 4! = 5 \\cdot 4 \\cdot 3 \\cdot 2 \\cdot 1 = 120$\n",
    "- By convention: $0! = 1$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "59c468f9",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "5 * 4 * 3 * 2 * 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24f848bc",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def factorial(n):\n",
    "    if n == 0:\n",
    "        return 1\n",
    "    else:\n",
    "        return n* factorial(n-1)\n",
    "    \n",
    "factorial(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6e35d6c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Recursion example 2: [Fibonacci series](https://oeis.org/A000045)\n",
    "\n",
    "- Definition: $$F(n) = F(n-1) + F(n-2)$$\n",
    "  where\n",
    "  - $n = 2, 3, 4, 5, \\dots$\n",
    "  - $F(0) = 0$\n",
    "  - $F(1) = 1$\n",
    "- [Related to *golden ratio*](https://en.wikipedia.org/wiki/Golden_ratio#Relationship_to_Fibonacci_and_Lucas_numbers), which has wide range of applications in the arts (music, architecture, etc.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "762b7764",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition\n",
    "def fibonacci(n):\n",
    "    if n <= 1:\n",
    "        return n\n",
    "    else:\n",
    "        # Recursion (function calls itself)\n",
    "        return(fibonacci(n-1) + fibonacci(n-2))\n",
    "\n",
    "# Main code body with recursive function call\n",
    "for i in range(10):\n",
    "    print(fibonacci(i))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a74328c4",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40dcf4e9",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise: Voltage level in dBu\n",
    "\n",
    "Write a function that computes the level $L_V$ of a given voltage $V$ in dBu."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d009f3bb",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$L_V = 20 \\cdot \\log_{10} \\left( \\frac{V}{V_0} \\right)$$\n",
    "\n",
    "where $V_0 = 0.7746$ V."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1ba21ac9",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Import required module\n",
    "from math import log10\n",
    "\n",
    "# Function definition\n",
    "def voltage_level_dbu(V):\n",
    "    V0 = 0.7746\n",
    "    L = 20 * log10(V/V0)\n",
    "    return L\n",
    "\n",
    "# Main code body with function call\n",
    "V = 1.5\n",
    "L = voltage_level_dbu(V)\n",
    "print(\"A voltage of\", V, \"V corresponds to\", round(L,1), \"dBu.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eccd1339",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise: Voltage level in dBV\n",
    "\n",
    "How would the previous function need to be changed to compute the level $L_V$ of a given voltage $V$ in dBV rather than dBu?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "639aafa3",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$L_V = 20 \\cdot \\log_{10} \\left( \\frac{V}{V_0} \\right)$$\n",
    "\n",
    "where $V_0 = 1$ V."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6705d3eb",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Import required module\n",
    "from math import log10\n",
    "\n",
    "# Function definition\n",
    "def voltage_level_dbv(V):\n",
    "    V0 = 1\n",
    "    L = 20 * log10(V/V0)\n",
    "    return L\n",
    "\n",
    "# Main code body with function call\n",
    "V = 2\n",
    "L = voltage_level_dbv(V)\n",
    "print(\"A voltage of\", V, \"V corresponds to\", round(L,1), \"dBu.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4db7a00",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise: Combining the two previous functions\n",
    "\n",
    "- How could we combine the previous two functions into a *single* function that is called like this:\n",
    "  ```\n",
    "  L = voltage_level(V,\"dBu\")\n",
    "  L = voltage_level(V,\"dBV\")\n",
    "  ```\n",
    "- Hint: Use an `if` statement inside the function body.\n",
    "- Function should default to dBu"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca9977f5",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Import required module\n",
    "from math import log10\n",
    "\n",
    "# Function definition\n",
    "def voltage_level(V, unit=\"dBu\"):\n",
    "    if unit == \"dBu\":\n",
    "        V0 = 0.7746\n",
    "    elif unit == \"dBV\":\n",
    "        V0 = 1\n",
    "    else:\n",
    "        print(\"You gave me garbage, user!\")\n",
    "    L = 20 * log10(V/V0)\n",
    "    return L    \n",
    "\n",
    "# Main code body with function calls\n",
    "L = voltage_level(1, \"dBu\")\n",
    "print(\"A voltage of\", V, \"V corresponds to\", round(L,1), \"dBu.\")\n",
    "\n",
    "L = voltage_level(1, \"dBV\")\n",
    "print(\"A voltage of\", V, \"V corresponds to\", round(L,1), \"dBV.\")\n",
    "\n",
    "L = voltage_level(1) # defaults to dBu\n",
    "print(\"A voltage of\", V, \"V corresponds to\", round(L,1), \"dBu.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e445adfc",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise: `samples2ms` function\n",
    "\n",
    "Write a function that converts a given number of samples to the corresponding time interval in milliseconds at a given sample rate in Hz that should default to 44100 Hz."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ad4d0c0",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$t = 1000 \\cdot \\frac{n}{f_s}$$\n",
    "\n",
    "where\n",
    "\n",
    "- $t$ ... time in ms\n",
    "- $n$ ... number of samples\n",
    "- $f_s$ ... sample rate in Hz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "74bac72e",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition\n",
    "def samples2ms(num_samples, sample_rate=44100):\n",
    "    time_ms = 1000 * num_samples / sample_rate\n",
    "    return time_ms\n",
    "\n",
    "# A function call using the sample rate's default value\n",
    "n = 44100\n",
    "time_ms = samples2ms(n)\n",
    "print('Assuming a sample rate of 44100 Hz,', n, 'samples correspond to', time_ms, 'milliseconds.')\n",
    "\n",
    "# Another function call using the sample rate's default value\n",
    "n = 88200\n",
    "time_ms = samples2ms(n)\n",
    "print('Assuming a sample rate of 44100 Hz,', n, 'samples correspond to', time_ms, 'milliseconds.')\n",
    "\n",
    "# A function call that explicitly specifies the sample rate\n",
    "n  = 48000\n",
    "fs = 96000\n",
    "time_ms = samples2ms(n, fs)\n",
    "print('Assuming a sample rate of', fs, 'Hz,', n, 'samples correspond to', time_ms, 'milliseconds.')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81442a74",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise: `get_bitrate` function\n",
    "\n",
    "Write a function that computes the bit rate of a digital audio signal at a given sample rate (Hz), bit depth (16, 24, etc.) and number of channels (1 for mono, 2 for stereo, etc.). The result should be given in Mbit/s. The function should default to CD-quality audio."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6cb36ef",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$B = \\frac{f_s \\cdot N \\cdot C}{10^6}$$\n",
    "\n",
    "where\n",
    "\n",
    "- $B$ ... bit rate in Mbit/s\n",
    "- $f_s$ ... sample rate in Hz\n",
    "- $N$ ... bit depth in bits\n",
    "- $C$ ... number of channels\n",
    "- $10^6$ ... mega"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ee0129ee",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Function definition\n",
    "def get_bitrate(sample_rate=44100, bit_depth=16, num_channels=2):\n",
    "    bit_rate = sample_rate * bit_depth * num_channels\n",
    "    bit_rate /= 1e6 # Convert from bit/s to Mbit/s\n",
    "    return bit_rate\n",
    "\n",
    "# Default to CD quality audio\n",
    "bit_rate = get_bitrate()\n",
    "print('The bit rate of an audio CD is', bit_rate, 'Mbit/s.')\n",
    "\n",
    "# Or specify parameters explicitly\n",
    "fs = 96000; N = 24; C = 8\n",
    "B = get_bitrate(fs, N, C)\n",
    "print('The bit rate of a', fs, 'Hz,', N, 'bit,', C, 'channel audio signal is', B, 'Mbit/s.')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3dc3bd78",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise: Converting MIDI note values to frequencies\n",
    "\n",
    "- Assume [equal temperament](https://en.wikipedia.org/wiki/Equal_temperament)\n",
    "- Use concert pitch of A4 = 440Hz (= MIDI note #69) as reference"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35096e12",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "$$f = 440 \\cdot 2^\\frac{m - 69}{12}$$\n",
    "\n",
    "- $f$ ... frequency (Hz)\n",
    "- $440$ ... concert pitch A4 (Hz)\n",
    "- $69$ ... concert pitch A4 (MIDI value)\n",
    "- $m$ ... MIDI note value whose frequency you want to find (0...127)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6d020ab6",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "def midi2freq(midi=69):\n",
    "    # NOTE: Concert pitch A4 at a frequency of 440 Hz corresponds to MIDI note #69.\n",
    "    frequency = 440 * 2**((midi - 69)/12) # assumes equal temperament\n",
    "    return frequency # in Hz\n",
    "\n",
    "print(midi2freq(69))\n",
    "print(midi2freq(69+12))\n",
    "print(midi2freq(69-12))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e23e036e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [Namespaces](https://realpython.com/python-namespaces-scope/)\n",
    "\n",
    "- A key programming concept\n",
    "- Python knows different *namespaces types*:\n",
    "  - Built-in\n",
    "  - Global\n",
    "  - Local\n",
    "  - Enclosing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4973d6ce",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Built-in namespace\n",
    "\n",
    "- Created when Python interpreter starts up\n",
    "- Exists until interpreter terminates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ed26be4",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Let's first reset the interpreter (IPython only!) for a clean slate\n",
    "# TODO: Not working as expected :(\n",
    "%reset -f"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c36b838",
   "metadata": {},
   "outputs": [],
   "source": [
    "# List objects in built-in namespace\n",
    "dir(__builtins__)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "515760f5",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "- You likely recognize some of these objects: `abs`, `max`, `min`, `int`, `str`, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "71ba0994",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Global namespace\n",
    "\n",
    "- Created when main program body starts\n",
    "- Exists until interpreter terminates\n",
    "- Interpreter also creates (separate) global namespace for each module loaded via `import`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b817caa2",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# List objects in global namespace\n",
    "globals()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4645cd13",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Note that this is a simple Python dictionary!\n",
    "type(globals())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb587627",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Let's add an object to the global namespace and repeat\n",
    "x = 'foo'\n",
    "\n",
    "globals()\n",
    "# Scroll to end of output to confirm addition of x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "651444d5",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# What is x?\n",
    "print(x)\n",
    "\n",
    "# Explicitly reference x *in global namespace*\n",
    "print(globals()['x'])\n",
    "\n",
    "# Confirm that these are one and the same *object* (not just: the same content!)\n",
    "x is globals()['x']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9188551",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "globals()['y'] = 100\n",
    "y"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c40b48e5",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "globals()['y'] = 3.14159\n",
    "y"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "513703f8",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Local namespace\n",
    "\n",
    "- Created when current function executes\n",
    "- Exists until current function terminates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70afd372",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "s = 'global'\n",
    "\n",
    "def f(x, y):\n",
    "    s = 'local'\n",
    "    \n",
    "    # List objects in local namespace\n",
    "    print(locals())\n",
    "\n",
    "# Function call\n",
    "f(10, 0.5)\n",
    "# Notice how:\n",
    "# - Arguments passed to function appear in its local namespace\n",
    "# - Variable 's' in global namespace is ignored\n",
    "\n",
    "# But now that f() has terminated, variable 's' is only defined in global namespace\n",
    "s"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35e4a0c6",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Enclosing namespace\n",
    "\n",
    "- Created when *enclosing* function executes\n",
    "- Exists until *enclosing* function terminates"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec06f5f5",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def f(x):\n",
    "    s = 'enclosing'\n",
    "    \n",
    "    def g(x):\n",
    "        s = 'local'\n",
    "    \n",
    "        # List objects in local namespace\n",
    "        print('Enclosed g() function:', locals())\n",
    "        \n",
    "    g(35)\n",
    "    \n",
    "    # List objects in local namespace\n",
    "    print('Enclosing f() function:', locals())\n",
    "\n",
    "# Function call\n",
    "f(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "670b7216",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [Scopes](https://www.w3schools.com/python/python_scope.asp)\n",
    "\n",
    "- It is important to consider the *scope* of a given variable!\n",
    "- *LEGB rule* defines order in which Python interpreter searches for a name:\n",
    "  1. **Local** scope defined by the current function\n",
    "  2. **Enclosing** scope of the parent function (in case of nested functions)\n",
    "  3. **Global** scope\n",
    "  4. **Built-in** scope\n",
    "- If name cannot be found in either of the above: `NameError` exception\n",
    "\n",
    "![LEGB Rule](pics/python_scopes.png)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58661db5",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Exercise 1 (LEGB rule): Which scope is `x` found in?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7fec3e82",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = 'global'\n",
    "\n",
    "def f():\n",
    "    def g():\n",
    "        print(x)\n",
    "    g()\n",
    "f()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4eefb07",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "1. Which function defines the *local* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4584dca1",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "2. Which function defines the *enclosing* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "399ab4b0",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "3. Which function defines the *global* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e670d97d",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "1. `x` is *not* defined in the local scope of the function `g()`.\n",
    "2. `x` is *not* defined in the enclosing scope of the function `f()`.\n",
    "3. `x` is defined in the **global** scope of the main program body.\n",
    "4. The built-in scope of the Python interpreter can thus be ignored."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "74605b00",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise 2 (LEGB rule): Which scope is `x` found in?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15cc1dd3",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = 'global'\n",
    "\n",
    "def f():\n",
    "    x = 'enclosing'\n",
    "    def g():\n",
    "        print(x)\n",
    "    g()\n",
    "f()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3730533",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "1. Which function defines the *local* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef87c435",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "2. Which function defines the *enclosing* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81ecf069",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "1. `x` is *not* defined in the local scope of the function `g()`.\n",
    "2. `x` is defined in the **enclosing** scope of the function `f()`.\n",
    "3. The global scope defined by the main program body can thus be ignored.\n",
    "4. The built-in scope of the Python interpreter can thus be ignored."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac48cf75",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise 3 (LEGB rule): Which scope is `x` found in?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "56afe48e",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = 'global'\n",
    "\n",
    "def f():\n",
    "    x = 'enclosing'\n",
    "    def g():\n",
    "        x = 'local' \n",
    "        print(x)\n",
    "    g()\n",
    "f()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c75f1d4",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "1. Which function defines the *local* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91fb475a",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "1. `x` is defined in the **local** scope of the function `g()`.\n",
    "2. The enclosing scope defined by the function `f()` can thus be ignored.\n",
    "3. The global scope defined by the main program body can thus be ignored.\n",
    "4. The built-in scope of the Python interpreter can thus be ignored."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93f833aa",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise 4 (LEGB rule): Which scope is `x` found in?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d832988f",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# What happens if we do not define x at all?\n",
    "# Requires interpreter reset (IPython only!), so previous definitions do not interfere!\n",
    "%reset -f\n",
    "\n",
    "def f():\n",
    "    def g():\n",
    "        print(x)\n",
    "    g()\n",
    "f()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae8b5408",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "1. Which function defines the *local* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d5ea7d5",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "2. Which function defines the *enclosing* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c51b0a2",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "3. Which function defines the *global* scope? Is `x` defined there?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2c1b41d",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "1. `x` is *not* defined in the local scope of the function `g()`.\n",
    "2. `x` is *not* defined in the enclosing scope of the function `f()`.\n",
    "3. `x` is *not* defined in the global scope of the main program body.\n",
    "4. `x` is *not* defined in the built-in scope of the Python interpreter.\n",
    "\n",
    "Hence, a `NameError` exception occurs."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e89902f",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Exercise 5: Local namespace beats built-in namespace"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37c4e8b1",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def foo():\n",
    "    float = 'hello'\n",
    "    print(float)\n",
    "    \n",
    "foo()\n",
    "help(float)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f87b508b",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Modifying variables out of scope\n",
    "\n",
    "- A function cannot modify an object of immutable type outside its local scope at all.\n",
    "- A function cannot *redefine* an object of mutable type outside its local scope, but can *modify* it in place."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b36fea51",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# A function cannot modify an immutable object outside its local scope.\n",
    "\n",
    "x = 20   # 'int' = immutable\n",
    "\n",
    "def f():\n",
    "    x = 40\n",
    "    print(x)\n",
    "\n",
    "f()\n",
    "x"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a7ba0308",
   "metadata": {},
   "outputs": [],
   "source": [
    "# A function cannot /redefine/ a mutable object outside its local scope.\n",
    "\n",
    "mics = ['sm58', 'sm57', 'c414'] # 'list' = mutable\n",
    "\n",
    "def f():\n",
    "    mics = ['c451', 'md421', 'm50']\n",
    "\n",
    "f()\n",
    "mics"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9d9b0f8f",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# But a function can /modify/ a mutable object outside its local scope.\n",
    "\n",
    "mics = ['sm58', 'sm57', 'c414'] # 'list' = mutable\n",
    "\n",
    "def f():\n",
    "    mics[1] = 'md421'\n",
    "\n",
    "f()\n",
    "mics"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10801344",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## The `global` declaration\n",
    "\n",
    "- A way to define global-scope objects *within a local scope*\n",
    "- *Not* generally recommended in practice!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2694fb36",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "%reset -f\n",
    "  \n",
    "def my_function():\n",
    "    global x\n",
    "    x = 300\n",
    "\n",
    "my_function()\n",
    "print(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79786617",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## The `nonlocal` declaration\n",
    "\n",
    "- A way to define enclosing-scope objects *within a local scope*\n",
    "- *Not* generally recommended in practice!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08ed1aa7",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "def f():\n",
    "    x = 20\n",
    "    \n",
    "    def g():\n",
    "        nonlocal x\n",
    "        x = 40\n",
    "        \n",
    "    g()\n",
    "    print(x)\n",
    "\n",
    "f()"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  },
  "rise": {
   "autolaunch": true,
   "enable_chalkboard": true,
   "reveal_shortcuts": {
    "main": {
     "fullscreenHelp": "x",
     "toggleOverview": "tab"
    }
   },
   "scroll": true,
   "theme": "serif"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
