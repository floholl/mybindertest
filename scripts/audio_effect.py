# 1. IMPORT REQUIRED MODULES

import soundfile as sf			# Requires pysoundfile package
import sounddevice as sd			# Requires python-sounddevice package
import numpy as np				# Requires numpy package
import matplotlib.pyplot as plt	# Requires matplotlib package


# 2. EFFECT FUNCTION DEFINITION

def earwracker(audio, fs, gain=3):
    audio = audio[fs:]					# Crop first second
    audio = audio[::-1] 					# Reverse
    audio = gain * audio					# Amplify
    audio = np.clip(audio, -0.1, 0.1)	# Distort
    return audio


# 3. MAIN CODE BODY

# Read sound file from disk to RAM
# NOTE: Requires mono.wav in parent folder of this .py file.
audio, fs = sf.read("../mono.wav")
print(fs)
print(len(audio)/fs)

# Process variable in RAM
audio_out = earwracker(audio, fs)
#audio_out = earwracker(audio, fs, 99999999)

# Play the result from RAM
sd.play(audio_out, fs)

# Write the result from RAM to disk
# NOTE: Writes file to same folder as this .py file
sf.write('out.wav', audio_out, fs)

# x axis that represents number of seconds (rather than samples)
x = np.linspace(0, len(audio_out)/fs, len(audio_out))

# Plot audio data against x axis (opens separate window)
plt.plot(x, audio_out)
plt.show()
