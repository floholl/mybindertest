"""
This script computes the first 'num_harmonics' in
a harmonic spectrum based on the 'fundamental' 
frequency.
"""

# Fundamental frequency in Hz
fundamental = 330

# Number of harmonics in the spectrum
num_harmonics = 4

# Compute frequencies of all harmonics
for i in range(num_harmonics):
    frequency = fundamental * (i+1)
    print(frequency)