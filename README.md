# PROG 110 · Art of Code · Audio: Python

This repository hosts [Dr. Florian Hollerweger](https://www.colum.edu/academics/faculty/detail/florian-hollerweger.html)'s [Jupyter Notebooks](https://en.wikipedia.org/wiki/Project_Jupyter#Jupyter_Notebook) for this class at Columbia College Chicago's [Department of Audio Arts and Acoustics](https://www.colum.edu/academics/media-arts/audio-arts-and-acoustics/). If run on a Jupyter kernel with the [RISE](https://rise.readthedocs.io) package installed, the notebooks will automatically launch into a [Reveal.js](https://revealjs.com/) slideshow. There are three use cases for these notebooks:

- Although you can **view** the Jupyter Notebooks directly here on this Git repository, you will neither be able to run any Python code that they contain in this fashion nor put them into slideshow mode.
- I therefore recommend to instead **download** the notebooks and run them on a Jupyter kernel on your local machine, such as the one included in the [Anaconda Distribution](https://www.anaconda.com/products/distribution).
- Alternatively, you can **launch** the notebooks online, using a Jupyter kernel on a Docker container provided by [Binder](https://mybinder.org). All you need for this is a web browser (and some patience).

Detailed instructions for the latter two use cases can be found below the following overview table.


## Fall 2023 (instructor: Dom Bonelli, MA)

This semester's class will be taught by Dom Bonelli. Please note that during the semester, any updates to the Jupyter Notebooks will *not* be merged into this GitLab repository here. Rather, updated versions of the notebooks will be disseminated through the Canvas pages for this class (access for enrolled students only):

- Section 01 (Wed, 9:00--11:50am): <https://canvas.colum.edu/courses/34149>
- Section 02 (Wed, 12:30--3:20pm): <https://canvas.colum.edu/courses/34153>

If you are a student enrolled in one of these two sections, please refer to your section's Canvas page for the most up-to-date version of the notebooks.

| Session | Date         | Topic | Notebook | | |
|--------:|--------------|------------------|-------------|-----------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
|       1 | Sep  6, 2023 | Introduction                                       | [view](prog110_class_01.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_01.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_01.ipynb) |
|       2 | Sep 13, 2023 | Flow control: branching & iteration                | [view](prog110_class_02.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_02.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_02.ipynb) |
|       3 | Sep 20, 2023 | Collections & scripts                              | [view](prog110_class_03.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_03.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_03.ipynb) |
|       4 | Sep 27, 2023 | Functions, namespaces & scopes                     | [view](prog110_class_04.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_04.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_04.ipynb) |
|       5 | Oct  4, 2023 | Better code: read & write documentation; debugging | [view](prog110_class_05.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_05.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_05.ipynb) |
|       6 | Oct 11, 2023 | Numeric arrays with NumPy                          | [view](prog110_class_06.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_06.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_06.ipynb) |
|       7 | Oct 18, 2023 | Recording & playing sound in Python                | [view](prog110_class_07.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_07.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_07.ipynb) |
|       8 | Oct 25, 2023 | Sound processing in Python                         | [view](prog110_class_08.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_08.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_08.ipynb) |
|       9 | Nov  1, 2023 | Classes: object-oriented programming               | [view](prog110_class_09.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_09.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_09.ipynb) |
|      10 | Nov  8, 2023 | Sound synthesis in Python                          | [view](prog110_class_10.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_10.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_10.ipynb) |
|      11 | Nov 15, 2023 | Better code: organizing larger projects & testing  | [view](prog110_class_11.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_11.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_11.ipynb) |
|      12 | Nov 29, 2023 |  Sound analysis in Python                  | [view](prog110_class_12.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_12.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_12.ipynb) |
|      13 | Dec  6, 2023 | Final project presentations                        | N/A | N/A | N/A |
|      14 | Dec 13, 2023 | The Fast Fourier Transform (FFT) |[view](prog110_class_14.ipynb) | [download](https://gitlab.com/floholl/art-of-code/-/raw/master/prog110_class_14.ipynb?inline=false) | [launch](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks%2Fprog110_class_14.ipynb) |


## Instructions for online use (*launch*)

The online version of the notebooks relies on [Binder](https://mybinder.org) to create a fully interactive version of the notebooks on the web directly from our Git repository. While they are not as responsive and might take several minutes to load, they do not rely on any infrastructure other than a web browser. This means you can view them in slideshow mode and run the Python code embedded in them also on machines that do not have the Anaconda Distribution or any other Jupyter Notebook kernel installed.

1. Use the *launch* links in the table above to load the notebook you are interested in.
2. Wait for a few minutes for the Binder to load (you should see some printout in a black log window, but more rewarding to grab a coffee).
3. After ca. 3-10 minutes (but sometimes up to 25 minutes; or sometimes within less than a minute), the Jupyter Notebook should open and launch right into the slideshow.

Rather than launching a specific notebook, it may be preferable to [launch an overview of all notebooks](https://mybinder.org/v2/gl/floholl%2Fart-of-code/HEAD?urlpath=%2Fnotebooks) to pick from. Note that you can access this overview also after launching a specific notebook by exiting the slideshow into the 'plain' Jupyter Notebook GUI and then selecting *File > Open...*.


## Instructions for local use (*download*)

For faster response times, I recommend running the `.ipynb` files on a local Jupyter Notebook kernel, such as the one provided by the *Anaconda Distribution*. This is a multi-step process.

### Install Anaconda Distribution with required packages
Local use of the notebooks requires you to (once) set up a suitable software environment.

1. Install the [Anaconda Distribution](https://www.anaconda.com/products/distribution) for your operating system.
2. Install the `rise` package from the `conda-forge` channel. Two options:
   - If you use macOS or GNU/Linux and are comfortable with command-line interfaces (CLIs) such as macOS' *Applications > Utilities > Terminal*:
     ```bash
     $ conda install -c conda-forge rise
     ```
   - If you use Windows or are more comfortable with graphical user interfaces (GUIs):
     1. Open the [Anaconda Navigator](https://docs.anaconda.com/anaconda/navigator/).
     2. Environments > Channels > Add...: `conda-forge`
     3. Change `Installed` to `All` in drop-down menu
     4. Search Packages: `rise`
     5. Select `rise` package and install

Note that additional conda packages may need to be installed for *all* code in the notebooks to successfully run. Instructions will be provided on a case-by-case basis.

### Download a single notebook
Next, you need to download the notebook of interest from this Git repository to your local hard drive:

1. Use the *Download* links in the table above to retrieve a specific notebook.
2. A file saving dialog opens. Save the notebook as an `.ipynb` file to a location you'll remember.

### Open the downloaded notebook
Now you need to run the downloaded notebook on your local Jupyter kernel:

1. Open the *Anaconda Navigator*
2. Launch the *Jupyter Notebook* (≠ *Jupyter Lab*)
3. A file browser window should open in your web browser.
   1. Locate the previously downloaded `.ipynb` file on your local hard drive and click on it.
   2. The Jupyter Notebook should open and launch right into the slideshow.

### Clone the entire repository and keep it updated
Even though you can continue to use the above method of downloading individual `.ipynb` notebooks, it might be more convenient to instead rely on the `git` command to first clone the entire repository of notebooks and later automatically retrieve updates to it. This should be straightforward for macOS and GNU/Linux users. Windows users may need to first install [Git for Windows](https://gitforwindows.org/).

1. Open a Terminal window to clone the repository *once* before first-time use:
   ```bash
   $ git clone https://gitlab.com/floholl/art-of-code.git
   ```
   This will result in a new folder `art-of-code` in your current working directory that includes all `.ipynb` notebooks.
2. Expect continuous updates to the notebooks throughout the semester. To make sure your local copy of the repository is up to date, I recommend to run the following command sequence at least once per week:
   ```bash
   $ cd /path/to/local/copy/of/art-of-code/
   $ git pull
   ```
   If you have made changes to your local copy of the notebooks since the last update, you may have to stash them before the pull:
   ```bash
   $ git stash
   $ git pull
   ```


## Using the notebooks (via download or launch)

### Editing and running embedded Python code
To edit, click in the code box window and type away. To run code, click `shift+return`. Note that some code (e.g., for embedded YouTube videos) might take a few seconds to execute, especially in the (slower) online version of the notebooks. *Not all Python code examples are fully self-contained!* In general, if you move sequentially through the slides, running each code examples as you go, things should work, but don't expect code to 'just work' if you skip ahead (where code blocks might rely on the execution of previous ones).

### Exiting and re-entering slideshow mode
- To exit from the slideshow into the 'plain' Jupyter Notebook, click the circled X in the top left of the window.
- To re-enter the slideshow, use the `alt+R` shortcut or click on the button whose mouse-over context help reads *Enter/Exit RISE Slideshow*. It is located to the right of the drop-down menu that usually is set to *Markdown* and looks like a vertical bar chart. If this button is missing, you are lacking the `rise` package from the `conda-forge` channel (see above for details).

### Navigating the slides
- Move through the slides using the `space` and `shift+space` key combinations. Click the `?` icon for further useful keyboard shortcuts.
- I recommend that you put your browser into *fullscreen mode* while viewing the notebook in slideshow mode.
  - Firefox on macOS: *View > Enter Full Screen* or `cmd+F` (extra steps required if you want to also hide menu and tool bars)
  - Safari & Chrome on macOS: *View > Enter Full Screen* or `fn+F`
  - Firefox on Windows: *View > Enter Full Screen* or `F11`
- Depending on your screen size and resolution, you may need to *vertically scroll* on some slides to see content towards their bottom that will otherwise remain hidden; don't miss out on it! You can also use `cmd++` and `cmd+-` (macOS) or `ctrl++` and `ctrl+-` (Windows) to match the slides to your screen. 
