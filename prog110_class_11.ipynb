{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "005ba935",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# PROG 110 · Art of Code · Audio: Python\n",
    "## Session 11: Better code: organizing larger projects & testing\n",
    "## Fall 2023 · November 15\n",
    "### Columbia College Chicago · Audio Arts & Acoustics\n",
    "### Dr. Florian Hollerweger · [✉](mailto:fhollerweger@colum.edu) [☏](https://teams.microsoft.com/l/chat/0/0?users=%3Cfhollerweger@colum.edu%3E) [📅](https://outlook.office365.com/owa/calendar/AudioArtsandAcoustics@office365.colum.edu/bookings/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58def255",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [Modular programming](https://realpython.com/python-modules-packages/)\n",
    "\n",
    "![xkcd comic #844: Good code](https://imgs.xkcd.com/comics/good_code.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e40aaeb",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Advantages of modular programming\n",
    "\n",
    "- **Simplicity**: Define complex problems in terms of smaller, simpler ones.\n",
    "- **Maintainability**: Modifications will have limited impact on other parts of the program.\n",
    "- **Reusability**: Eliminates the need to duplicate code.\n",
    "- **Scoping**: Helps avoid collisions between identifiers in different areas of a program.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d476183",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Vehicles for modular programming\n",
    "\n",
    "- [Functions](https://www.w3schools.com/python/python_functions.asp) were discussed in an earlier lecture.\n",
    "- [Classes](https://www.w3schools.com/python/python_classes.asp) were discussed in an earlier lecture.\n",
    "- [Modules](https://www.w3schools.com/python/python_modules.asp): A single `.py` file with a collection of functions, classes, etc. that form a logical unit.\n",
    "- [Packages](https://docs.python.org/3/tutorial/modules.html#packages): A directory with a hierarchical collection of modules and sub-packages and an `__init__.py` file.\n",
    "\n",
    "We can import all objects in a module with the `*` operator, but we can’t import all modules in a package at once."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50f9526e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [Module types](https://realpython.com/python-modules-packages/#python-modules-overview)\n",
    "\n",
    "\n",
    "- **Built-in modules** contained in the Python interpreter\n",
    "  - Example: `platform`\n",
    "- **Modules written in C** and loaded dynamically at run-time\n",
    "  - Example: `numpy`\n",
    "- **Modules written in Python** itself\n",
    "  - Contained in a single `.py` file\n",
    "  - Our main focus, since we can write them ourselves. :)\n",
    "\n",
    "All three types of modules are loaded with the `import` statement."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "76cc2306",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Darwin\n"
     ]
    }
   ],
   "source": [
    "# Load the built-in 'platform' module\n",
    "import platform\n",
    "\n",
    "# Use its system() function to print info on the current operating system\n",
    "my_system = platform.system()\n",
    "print(my_system)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d7de481",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [Importing modules written in Python](https://csatlas.com/python-import-file-module/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2791cf26",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Import a module from the same directory](https://csatlas.com/python-import-file-module/#import_a_file_in_the_same_directory)\n",
    "\n",
    "Consider the following directory & file structure:\n",
    "\n",
    "    my_dir/\n",
    "    |\n",
    "    |-- my_module.py\n",
    "    |   |\n",
    "    |   |-- def my_function():\n",
    "    |\n",
    "    |-- script.py\n",
    "    \n",
    "To use `my_function()`, use this code in `script.py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2e3b5e06",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import my_module\n",
    "my_module.my_function()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1bd1d8f1",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Import a module from a subdirectory](https://csatlas.com/python-import-file-module/#import_a_file_in_a_subdirectory_python_3_3_and_up)\n",
    "\n",
    "Consider the following directory & file structure:\n",
    "\n",
    "    my_dir/\n",
    "    |\n",
    "    |-- my_subdir/\n",
    "    |   |\n",
    "    |   |-- my_module.py\n",
    "    |       |    \n",
    "    |       |-- def my_function():\n",
    "    |\n",
    "    |-- script.py\n",
    "    \n",
    "To use `my_function()`, use this code in `script.py`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "273c632d",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import my_subdir.my_module\n",
    "my_subdir.my_module.my_function()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9e3881fc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Of course this also works:\n",
    "import subdir.module as m\n",
    "m.my_function()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4994971",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Packages"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04678178",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## The [Python Package Index (PyPI)](https://pypi.org/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "278f9379",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Installing packages from PyPi](https://docs.python.org/3/installing) with `pip`\n",
    "\n",
    "- Note that `pip` conflicts with `conda`!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "783592c1",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Distributing modules or packages on PyPI](https://docs.python.org/3/distributing)\n",
    "\n",
    "- https://docs.python.org/3/distributing\n",
    "- https://packaging.python.org\n",
    "- https://packaging.python.org/en/latest/tutorials/packaging-projects/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6621318a",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Coding style guides\n",
    "\n",
    "![xkcd comic #1513: Code quality](https://imgs.xkcd.com/comics/code_quality.png)\n",
    "\n",
    "- [PEP8: Style Guide for Python Code](https://peps.python.org/pep-0008/)\n",
    "  > A style guide is about consistency. Consistency with [a] style guide is important. Consistency within a project is more important. Consistency within one module or function is the most important.\n",
    "- [Fundamentals of Music Processing style guide](https://www.audiolabs-erlangen.de/resources/MIR/FMP/B/B_PythonStyleGuide.html)\n",
    "- [`music21` style guide](https://github.com/cuthbertLab/music21/blob/master/CONTRIBUTING.md#style-guide)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47f83448",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Testing code\n",
    "\n",
    "Guttag 2016, section 6.1 (pp. 85ff)\n",
    "\n",
    "- *Test suite*: Collection on inputs that will likely reveal bugs, but not take too long to run.\n",
    "- **Black-box testing**: without looking at code\n",
    "- **Glass-box testing**: with looking at code\n",
    "  - *Path-complete* if every potential path through program is exercised\n",
    "  - Unit testing\n",
    "  - Integration testing\n",
    "  - Software quality assurance (SQA)\n",
    "  - Stubs vs. drivers"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3dc8c3cd",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## The [`assert`](https://www.w3schools.com/python/ref_keyword_assert.asp) keyword\n",
    "\n",
    "- Useful for debugging\n",
    "- Allows you to ensure that your code does what you think it does & track down errors"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "04869804",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "mic = \"SM58\"\n",
    "\n",
    "# Returns True and does nothing else (all is well)\n",
    "assert mic == \"SM58\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21c50c2f",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Returns False and raises AssertionError\n",
    "assert mic == \"SM57\", \"mic should be 'SM58'\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3cbeeb58",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "x = \"hello\"\n",
    "\n",
    "# Returns False and raises AssertionError\n",
    "assert x == \"goodbye\", \"x should be 'hello'\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df06095f",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Exception handling](https://www.w3schools.com/python/python_try_except.asp): `try`, `except`, `else`, `finally`, `with` & `raise`\n",
    "\n",
    "- An 'insurance policy' to gracefully handle errors in your code\n",
    "- Example: user inputs data of incorrect type\n",
    "- Syntax:\n",
    "  - `try` block: tests block of code for errors\n",
    "  - `except` block: handles errors in code\n",
    "  - `else` block: executes code when there is no error\n",
    "  - `finally` block: executes code, regardless of `try` and `except` block results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5adbadaa",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# This will throw an error (what kind?), since 'x' is not defined\n",
    "print(x) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be63c4ab",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# This handles the error gracefully\n",
    "try:\n",
    "    print(x)\n",
    "except:\n",
    "    print(\"An exception occurred\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0058710",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# One can distinguish errors by type and have multiple 'except' blocks for each\n",
    "try:\n",
    "    print(x)\n",
    "except NameError:\n",
    "    print(\"Variable x is not defined\")\n",
    "except:\n",
    "    print(\"Something else went wrong\") "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e64cd6c",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    print(\"Hello\")\n",
    "except:\n",
    "    print(\"Something went wrong\")\n",
    "else:\n",
    "    print(\"Nothing went wrong\") "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8e5899f7",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    print(x)\n",
    "except:\n",
    "    print(\"Something went wrong\")\n",
    "finally:\n",
    "    print(\"The 'try except' is finished\") "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "210bd402",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "`finally` can be useful to close objects and clean up resources:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8b7ebcf3",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Try to open and write to a file that is not writable:\n",
    "try:\n",
    "    f = open(\"demofile.txt\")\n",
    "    try:\n",
    "        f.write(\"Lorem Ipsum\")\n",
    "    except:\n",
    "        print(\"Something went wrong when writing to the file\")\n",
    "    finally:\n",
    "        f.close()\n",
    "except:\n",
    "    print(\"Something went wrong when opening the file\") "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ad3af87b",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "file = open(\"my.txt\", \"w\")\n",
    "\n",
    "try:\n",
    "    file.write(\"Hello, world!\")\n",
    "finally:\n",
    "    file.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c41c39b1",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "The previous `try`/`finally` block can be shortened using [Python's `with` keyword](https://builtin.com/software-engineering-perspectives/what-is-with-statement-python):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "9669d61a",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "with open(\"my.txt\", \"w\") as file:\n",
    "    file.write(\"Hello, world!\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ddfd387e",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Throw (raise) an exception if a condition occurs\n",
    "\n",
    "x = \"hello\"\n",
    "\n",
    "if not type(x) is int:\n",
    "    raise TypeError(\"Only integers are allowed\") "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3bcb95cf",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Doc tests](https://docs.python.org/3/library/doctest.html)\n",
    "\n",
    "- Used by `music21`, for example.\n",
    "- [More info](https://mattermost.com/blog/testing-python-understanding-doctest-and-unittest/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3f6703f",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Unit tests](https://docs.python.org/3/library/unittest.html)\n",
    "\n",
    "- Used by `music21`, for example.\n",
    "- [More info](https://mattermost.com/blog/testing-python-understanding-doctest-and-unittest/)\n",
    "- [Nose](https://pypi.org/project/nose/) extends `unittest`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe7d3140",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# [Version/revision control](https://en.wikipedia.org/wiki/Version_control) systems\n",
    "\n",
    "- Centralized (client-server):\n",
    "  - [Concurrent Versions System (CVS)](https://savannah.nongnu.org/projects/cvs)\n",
    "  - [Apache Subversion (SVN)](https://subversion.apache.org/)\n",
    "- Distributed:\n",
    "  - [Mercurial](https://www.mercurial-scm.org)\n",
    "  - [Git](https://git-scm.com)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8a57407",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Git](https://git-scm.com/)\n",
    "\n",
    "![xkcd comic #1296: Git commit](https://imgs.xkcd.com/comics/git_commit.png)\n",
    "\n",
    "- A distributed version/revision control system\n",
    "- Online services (not *required*, but commonly used):\n",
    "  - [GitHub](https://github.com)\n",
    "  - [GitLab](https://gitlab.com/)\n",
    "  - [BitBucket](https://bitbucket.org/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c56c11d8",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Documentation generators"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80912fcb",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [Sphinx](https://www.sphinx-doc.org)\n",
    "\n",
    "- Can be combined with [GitHub](https://github.com/) and [ReadTheDocs](https://docs.readthedocs.io/en/stable/)\n",
    "- In addition to Python also supports Ada, C, C++, Fortran, JavaScript, PHP, Ruby\n",
    "- Uses the [reStructuredText (reST)](https://en.wikipedia.org/wiki/ReStructuredText) markup language"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "712ebf66",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## [MkDocs](https://www.mkdocs.org/)\n",
    "\n",
    "- A static site generator for building documentation\n",
    "- Can be combined with [GitHub](https://github.com/) and [ReadTheDocs](https://docs.readthedocs.io/en/stable/)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.7"
  },
  "rise": {
   "autolaunch": true,
   "enable_chalkboard": true,
   "reveal_shortcuts": {
    "main": {
     "fullscreenHelp": "x",
     "toggleOverview": "tab"
    }
   },
   "scroll": true,
   "theme": "serif"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
