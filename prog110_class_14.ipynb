{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "005ba935",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# PROG 110 · Art of Code · Audio: Python\n",
    "## Session 14: The Fast Fourier Transform (FFT)\n",
    "## Fall 2023 · December 13\n",
    "### Columbia College Chicago · Audio Arts & Acoustics\n",
    "### Dr. Florian Hollerweger · [✉](mailto:fhollerweger@colum.edu) [☏](https://teams.microsoft.com/l/chat/0/0?users=%3Cfhollerweger@colum.edu%3E) [📅](https://outlook.office365.com/owa/calendar/AudioArtsandAcoustics@office365.colum.edu/bookings/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "241317c7",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Theory"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e5847cf",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Time domain vs. frequency domain\n",
    "\n",
    "- Three ways to visualize sound:\n",
    "  - *Waveform* shows audio signal in **time domain** (amplitude as function of time)\n",
    "  - *Spectrum* shows audio signal in **frequency domain** (amplitude as function of frequency)\n",
    "  - *Spectrogram* shows signal in both domains (amplitude as function of time & frequency)\n",
    "- Some operations are more effectively implemented in the time domain. Examples:\n",
    "  - Delays\n",
    "  - Echoes\n",
    "- Other operations are more effectively implemented in the frequency domain. Examples:\n",
    "  - Certain filters\n",
    "  - Convolution of a signal with an impulse response (for artifical reverberation)\n",
    "  - Phase vocoders"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e17c0748",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Time domain vs. frequency domain\n",
    "\n",
    "![Spectral decomposition of a sawtooth wave](pics/fourier_series_sawtooth.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "93b4b83e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Real-valued vs. complex-valued signals\n",
    "\n",
    "- Two types of signals:\n",
    "  - **Real-valued signals** consist of 'regular' real numbers (eg., audio files).\n",
    "  - **Complex-valued signals** consist of complex numbers with a real and an imaginary part.\n",
    "- Why should you care about complex-valued signals?\n",
    "- Because processing even real-valued audio signals often renders them complex!\n",
    "  - Example: phase shifting\n",
    "  - So DSP processes internally often use complex-valued signals."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28890109",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Complex-valued signals\n",
    "\n",
    "- Complex number $z$ in cartesian ($a, b$) and polar ($r, \\varphi$) notation:\n",
    "  $$z = a + b \\cdot j = r \\cdot e^{j \\cdot \\varphi}$$\n",
    "- Conversions from cartesian to polar:\n",
    "  - Absolute value or *magnitude*: $r = \\left| z \\right|\t= \\sqrt{a^2 + b^2}$\n",
    "  - Angle or *phase*: $\\varphi = \\angle z = \\arctan \\left( \\frac{b}{a} \\right)$\n",
    "\n",
    "![Complex numbers](pics/complex_numbers.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "174aa104",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Fourier Transform glossary\n",
    "\n",
    "\n",
    "| Acronym | Meaning                    | Description                                                       |\n",
    "|---------|----------------------------|-------------------------------------------------------------------|\n",
    "| DFT     | Discrete Fourier Transform | Fourier transform for signals of discrete time & frequency        |\n",
    "| FFT     | Fast Fourier Transform     | a fast algorithm for implementing the DFT in practice             |\n",
    "| IFFT    | Inverse FFT                | inverse operation of the FFT                                      |\n",
    "| RFFT    | Real-valued FFT            | a more efficient implementation of the FFT for real-valued inputs |\n",
    "| IRFFT   | Inverse real-valued FFT    | inverse operation of the RFFT                                     |\n",
    "\n",
    "- DFT: Fourier transform for signals that are...\n",
    "  - finite in duration & bandwidth (but 'pretends' they are periodic)\n",
    "  - Includes most digital audio signals for (our) practical purposes\n",
    "- RFFT:\n",
    "  - Exploits symmetries in real-valued signals to speed up FFT\n",
    "  - Digital audio *files* (but not all audio *signals*!) are real-valued.\n",
    "  - Hence, the **(I)RFFT is what we will mostly use**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61871311",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# FFT in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db5c2828",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Python implementations\n",
    "\n",
    "Three popular modules with FFT implementations for Python:\n",
    "\n",
    "- **`scipy.fft` is what you should generally use**.\n",
    "- `scipy.fftpack` is [considered legacy](https://realpython.com/python-scipy-fft/#scipyfft-vs-scipyfftpack).\n",
    "- `numpy.fft` is an alternative where SciPy is not available. However:\n",
    "   - `numpy.fft` [will less likely receive bug fixes](https://realpython.com/python-scipy-fft/#scipyfft-vs-numpyfft).\n",
    "   - As a numerical operation, the FFT [better fits the SciPy use case](https://scipy.org/faq/#what-is-the-difference-between-numpy-and-scipy)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66c7b0a4",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## SciPy installation\n",
    "\n",
    "- **Anaconda**: should be pre-installed, but if not:\n",
    "  1. Start Anaconda Navigator\n",
    "  2. *Environments > Update index...*\n",
    "  3. Search Packages (*Not installed*) > `scipy`\n",
    "  4. Select package and `Apply`\n",
    "- **Thonny**: should be pre-installed, but if not:\n",
    "  1. Start Thonny\n",
    "  2. *Tools > Manage packages...*\n",
    "  3. Search on PyPi: `scipy`\n",
    "  4. Click on `scipy` & `Install`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e57d8033",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Recommended tools\n",
    "\n",
    "| Acronym | Meaning                    | Recommended tool  | Python use case                                  |\n",
    "|---------|----------------------------|-------------------|--------------------------------------------------|\n",
    "| FFT     | Fast Fourier Transform     | `scipy.fft.fft`   | higher-level DSP on complex-valued audio signals |\n",
    "| IFFT    | Inverse FFT                | `scipy.fft.ifft`  | higher-level DSP on complex-valued audio signals |\n",
    "| RFFT    | Real-valued FFT            | `scipy.fft.rfft`  | basic DSP on 'regular' audio signals             |\n",
    "| IRFFT   | Inverse real-valued FFT    | `scipy.fft.irfft` | basic DSP on 'regular' audio signals             |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a66ba28",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## FFT vs. RFFT in SciPy\n",
    "\n",
    "![FFt/IFFT vs. RFFT/IRFFT](pics/fft_ifft_vs_rfft_irfft.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5fb88418",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f5f4c55",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Demonstration of FFT vs. RFFT"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5601f092",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.fft import fft, rfft, ifft, irfft\n",
    "\n",
    "a = np.arange(4) # NOTE: Change 4 to 5 (or some other odd number) and re-run the code\n",
    "print('Input signal:', len(a), 'values')\n",
    "print(a)\n",
    "print('Are they complex?', np.iscomplexobj(a), '\\n')\n",
    "\n",
    "n = len(a)\n",
    "print('Expected FFT output:\\t', n, 'complex values')\n",
    "print('Expected IFFT output:\\t', n, 'complex values')\n",
    "\n",
    "if n % 2 == 0:  # if n is even\n",
    "    k = n/2 + 1\n",
    "else:           # if n is odd\n",
    "    k = (n+1)/2\n",
    "print('Expected RFFT output:\\t', int(k), 'complex values')\n",
    "print('Expected IRFFT output:\\t', int(2*(k-1)), 'real values\\n')\n",
    "\n",
    "# FFT-IFFT chain\n",
    "b = fft(a)\n",
    "print('FFT output:', len(b), 'values')\n",
    "print(b)\n",
    "print('Are they complex-valued?', np.iscomplexobj(b), '\\n')\n",
    "\n",
    "c = ifft(b)\n",
    "print('IFFT output:', len(c), 'values')\n",
    "print(c)\n",
    "print('Are they complex-valued?', np.iscomplexobj(c), '\\n')\n",
    "\n",
    "# RFFT-IRFFT chain\n",
    "d = rfft(a)\n",
    "print('RFFT output:', len(d), 'values')\n",
    "print(d)\n",
    "print('Are they complex-valued?', np.iscomplexobj(d), '\\n')\n",
    "\n",
    "e = irfft(d)\n",
    "print('IRFFT output:', len(e), 'values')\n",
    "print(e)\n",
    "print('Are they real-valued?', np.isrealobj(e), '\\n')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "21ba1ba2",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Note difference in loop execution time (Jupyter Notebooks or iPython terminal only)\n",
    "%timeit [fft(a)]\n",
    "%timeit [rfft(a)]\n",
    "# NOTE: Give this code a few seconds to print its results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0417f668",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Plotting the magnitude spectrum of an audio file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b234ced7",
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.fft import rfft, rfftfreq\n",
    "import soundfile as sf\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Load audio file\n",
    "audio, fs = sf.read('mono.wav')\n",
    "print('Number of samples:', len(audio))\n",
    "\n",
    "# Perform RFFT\n",
    "fft = rfft(audio)\n",
    "print('Number of frequency bins:', len(fft))\n",
    "\n",
    "# Calculate abs values on complex numbers to get magnitude spectrum\n",
    "magnitude_spectrum = np.abs(fft)\n",
    "# NOTE: Absolute value of a complex number = Pythagorean theorem on its real and imaginary parts:\n",
    "# sqrt(Re^2 + Im^2)\n",
    "\n",
    "# Create frequency variable\n",
    "freq = rfftfreq(len(audio), 1/fs)\n",
    "# f = np.linspace(0, fs/2, len(spectrum)) # Equivalent method\n",
    "print('Frequency of last bin:', freq[-1], 'Hz')\n",
    "\n",
    "# Plot spectrum\n",
    "plt.plot(freq, magnitude_spectrum) # NOTE: Also try plt.stem(...) instead of plt.plot(...)\n",
    "plt.title(\"Magnitude spectrum\")\n",
    "plt.xlabel(\"Frequency\")\n",
    "plt.ylabel(\"Magnitude\")\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f01b8fa",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Plotting the magnitude spectrum of a synthesized sine tone"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8055be6f",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.fft import fft, rfft, rfftfreq\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "\n",
    "### SYNTHESIZED TONE (SEE PREVIOUS LECTURES FOR DETAILS)\n",
    "\n",
    "# Global variables\n",
    "T  = 3     # duration in seconds (check out how changing this number changes power spectrum amplitudes!)\n",
    "#T = 0.001 # NOTE: What happens if you change T to this value instead? Why?\n",
    "fs = 44100 # sample rate in Hz\n",
    "\n",
    "# Duration in samples\n",
    "N = T * fs\n",
    "\n",
    "# Sample index n = t*fs from 0 to N-1\n",
    "n = np.arange(N)\n",
    "\n",
    "def get_omega(f, fs):\n",
    "    ωc = 2 * np.pi * f / fs\n",
    "    return(ωc)\n",
    "\n",
    "# Waveform properties\n",
    "f = 5000 # Hz\n",
    "\n",
    "# Get normalized angular frequency\n",
    "ω = get_omega(f, fs)\n",
    "\n",
    "# Compute amplitude for each sample\n",
    "audio = np.sin(ω * n)\n",
    "print('Number of samples:', len(audio))\n",
    "\n",
    "### FFT PART\n",
    "\n",
    "# Perform RFFT\n",
    "fft = rfft(audio)\n",
    "print('Number of frequency bins:', len(fft))\n",
    "\n",
    "# Calculate abs values on complex numbers to get magnitude\n",
    "magnitude_spectrum = np.abs(fft)\n",
    "# NOTE: Absolute value of a complex number = Pythagorean theorem on its real and imaginary parts:\n",
    "# sqrt(Re^2 + Im^2)\n",
    "\n",
    "# Create frequency variable\n",
    "freq = rfftfreq(len(audio), 1/fs)\n",
    "# f = np.linspace(0, fs/2, len(spectrum)) # Equivalent method\n",
    "print('Frequency of last bin:', freq[-1], 'Hz')\n",
    "\n",
    "# Plot spectrum\n",
    "plt.stem(freq, magnitude_spectrum)\n",
    "plt.title(\"Magnitude spectrum\")\n",
    "plt.xlabel(\"Frequency\")\n",
    "plt.ylabel(\"Magnitude\")\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c509901",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Plotting the phase spectrum of a synthesized sine tone"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b4dd1c1",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "phase_spectrum = np.angle(fft)\n",
    "# NOTE: Angle of a complex number: arctan(Im/Re)\n",
    "\n",
    "# Plot first 100 samples of the spectrum\n",
    "plt.plot(freq[0:100], phase_spectrum[0:100]) # NOTE: Also try plt.stem(...) instead of plt.plot(...)\n",
    "plt.title(\"Phase spectrum\")\n",
    "plt.xlabel(\"Frequency\")\n",
    "plt.ylabel(\"Phase\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebde0e2d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Plotting the power spectrum of a synthesized two-frequency sound"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48480c91",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.fft import rfft, rfftfreq\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import Audio # HTML5 player (Jupyter Notebooks only)\n",
    "\n",
    "\n",
    "### SYNTHESIZED TONE (SEE PREVIOUS LECTURES FOR DETAILS)\n",
    "\n",
    "# Global variables\n",
    "T  = 3     # duration in seconds (check out how changing this number changes power spectrum amplitudes!)\n",
    "fs = 44100 # sample rate in Hz\n",
    "\n",
    "# Duration in samples\n",
    "N = T * fs\n",
    "\n",
    "# Sample index n = t*fs from 0 to N-1\n",
    "n = np.arange(N)\n",
    "\n",
    "def get_omega(f, fs):\n",
    "    ωc = 2 * np.pi * f / fs\n",
    "    return(ωc)\n",
    "\n",
    "# Waveform frequencies\n",
    "f = np.array([1000, 2300])   # Hz\n",
    "f = f[:,np.newaxis] # convert (N,) to (N,1) array to allow for later broadcasting\n",
    "#f = f.reshape(-1,1) # alternative to previous line\n",
    "\n",
    "# Waveform amplitudes\n",
    "A = np.array([0.5, 0.25])\n",
    "A = A[:,np.newaxis] # convert (N,) to (N,1) array to allow for later broadcasting\n",
    "#A = A.reshape(-1,1) # alternative to previous line\n",
    "\n",
    "# Get normalized angular frequency\n",
    "ω = get_omega(f, fs)\n",
    "\n",
    "# Compute amplitude for each sample\n",
    "audio = A * np.sin(ω * n)\n",
    "print('Number of samples:', len(audio))\n",
    "\n",
    "# Mix all harmmonics by summation over dimension no. 0 (as counted by computers)\n",
    "audio = np.sum(audio, 0)\n",
    "\n",
    "\n",
    "### FFT PART\n",
    "\n",
    "# Perform RFFT\n",
    "fft = rfft(audio)\n",
    "print('Number of frequency bins:', len(fft))\n",
    "\n",
    "# Calculate abs values on complex numbers to get magnitude\n",
    "magnitude_spectrum = np.abs(fft)\n",
    "# NOTE: Absolute value of a complex number = Pythagorean theorem on its real and imaginary parts:\n",
    "# sqrt(Re^2 + Im^2)\n",
    "\n",
    "# Create frequency variable\n",
    "freq = rfftfreq(len(audio), 1/fs)\n",
    "# f = np.linspace(0, fs/2, len(spectrum)) # Equivalent method\n",
    "print('Frequency of last bin:', freq[-1], 'Hz')\n",
    "\n",
    "# Plot spectrum\n",
    "plt.stem(freq, magnitude_spectrum)\n",
    "plt.title(\"Magnitude spectrum\")\n",
    "plt.xlabel(\"Frequency\")\n",
    "plt.ylabel(\"Magnitude\")\n",
    "\n",
    "# Thonny users: uncomment the following line for plot to show\n",
    "#plt.show()\n",
    "\n",
    "# HTML5 player (Jupyter Notebooks only)\n",
    "Audio(audio, rate=fs, normalize=False)\n",
    "# NOTE: Needs to go after plot"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e42497d",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Filtering the signal in the frequency domain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "92456cd1",
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from scipy.fft import irfft\n",
    "\n",
    "# Get indices of non-zero spectral bins\n",
    "idx = np.where(magnitude_spectrum > 0.1)\n",
    "# NOTE: idx = np.nonzero(spectrum) # won't work due to numric round-off errors\n",
    "\n",
    "# Print the result: bins no. 3000 (= 1kHz) and 6900 (= 2.3kHz) are non-zero\n",
    "print(idx)\n",
    "\n",
    "# Set bin no. 6900 (= 2.3kHz) of RFFT output (/not/ of spectrum!) to zero\n",
    "fft[6900] = 0\n",
    "\n",
    "# Re-compute spectrum\n",
    "magnitude_spectrum = np.abs(fft)\n",
    "# NOTE: Absolute value of a complex number = Pythagorean theorem on its real and imaginary parts:\n",
    "# sqrt(Re^2 + Im^2)\n",
    "\n",
    "# Plot new spectrum (\"Look ma, no 2.3kHz!\")\n",
    "plt.stem(freq, magnitude_spectrum)\n",
    "plt.title(\"Magnitude spectrum\")\n",
    "plt.xlabel(\"Frequency\")\n",
    "plt.ylabel(\"Magnitude\")\n",
    "\n",
    "# Perform the inverse RFFT on the such processed RFFT output to get an audio signal\n",
    "audio_out = irfft(fft)\n",
    "\n",
    "# HTML5 player (Jupyter Notebooks only)\n",
    "Audio(audio_out, rate=fs, normalize=False)\n",
    "# NOTE: Needs to go after plot"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11e768ab",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Further reading\n",
    "\n",
    "- https://greenteapress.com/wp/think-dsp/\n",
    "- https://pythonnumericalmethods.berkeley.edu/notebooks/chapter24.04-FFT-in-Python.html"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  },
  "rise": {
   "autolaunch": true,
   "enable_chalkboard": true,
   "reveal_shortcuts": {
    "main": {
     "fullscreenHelp": "x",
     "toggleOverview": "tab"
    }
   },
   "scroll": true,
   "theme": "serif"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
